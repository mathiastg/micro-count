package implementations

type Service map[string]int

func (service Service) Increment(key string) int {
	service[key]++
	return service[key]
}

func (service Service) Decrement(key string) int {
	service[key]--
	return service[key]
}

func (service Service) Get(key string) int {
	return service[key]
}

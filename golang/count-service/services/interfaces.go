package services

type CountService interface {
	Increment(s string) int
	Decrement(s string) int
	Get(s string) int
}

package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gleerup.io/count-service/v2/implementations"
	"gleerup.io/count-service/v2/services"
)

type model struct {
	Resource string `json:"resource"`
	Count    int    `json:"count"`
}

func main() {
	var service services.CountService

	impl := implementations.Service(make(map[string]int))
	service = &impl

	router := gin.Default()
	router.GET("/*resource", func(c *gin.Context) {
		res := c.Param("resource")
		c.IndentedJSON(http.StatusOK, model{res, service.Get(res)})
	})
	router.POST("/increment/:resource", func(c *gin.Context) {
		res := c.Param("resource")
		c.IndentedJSON(http.StatusOK, model{res, service.Increment(res)})
	})
	router.POST("/decrement/:resource", func(c *gin.Context) {
		res := c.Param("resource")
		c.IndentedJSON(http.StatusOK, model{res, service.Decrement(res)})
	})
	router.Run()

}

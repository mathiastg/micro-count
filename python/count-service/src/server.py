import sys
from flask import Flask
import json
from counter.counter import get_data
from save import periodic_save
import logging
import pathlib

app = Flask(__name__)
app.url_map.strict_slashes = False


@app.route("/increment", defaults={"topic": ""}, methods=["POST"])
@app.route("/increment/<topic>", methods=["POST"])
def increment(topic):
    get_data()[topic] = get_data().get(topic, 0) + 1
    return json.dumps({topic: get_data().get(topic)}), 201


@app.route("/decrement", defaults={"topic": ""}, methods=["POST"])
@app.route("/decrement/<topic>", methods=["POST"])
def decrement(topic):
    get_data()[topic] = get_data().get(topic, 0) - 1
    return json.dumps({topic: get_data().get(topic)}), 201


@app.route("/", defaults={"topic": ""}, methods=["GET"])
@app.route("/<topic>", methods=["GET"])
def get(topic=""):
    return json.dumps({topic: get_data().get(topic, 0)}), 201


if __name__ == "__main__":
    save_dir = sys.argv[1]
    if save_dir == None or save_dir == "":
        save_dir = pathlib.Path(__file__).parent.resolve()
    logging.basicConfig(
        filename=pathlib.Path(f"{save_dir}/LOGFILE.log").resolve(), level=logging.DEBUG
    )
    periodic_save.start_save(get_data, f"{save_dir}/SAVEFILE")
    app.run(host="0.0.0.0", port=5000)

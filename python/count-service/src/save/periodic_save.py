import threading
import time
import pathlib
from typing import Callable
import json


def __loop(dataprovider: Callable[[], dict], filepath: str, interval: int):
    while True:
        time.sleep(interval)
        with open(filepath, "w") as writer:
            json.dump(dataprovider(), writer, indent=4)


def start_save(
    dataprovider: Callable[[], dict],
    filepath: str = f"{pathlib.Path().resolve()}/SAVEFILE",
    interval=5,
):
    threading.Thread(
        target=__loop, args=(dataprovider, filepath, interval), daemon=True
    ).start()

import sys
from flask import Flask
from api import counter_api
from business_things import important

app = Flask(__name__)
app.url_map.strict_slashes = False


@app.route("/", methods=["GET"])
def visitors():
    response = counter_api.increment("visitors")
    return f'{response.get("visitors")}', 201


@app.route("/dosomething", methods=["GET"])
def business_logic():
    important.do_important()
    response = counter_api.increment("logic")
    if response.get("logic") > 9000:
        return "It's over 9000!!!", 500
    return f'{response.get("logic")}', 201


@app.route("/exit", methods=["GET"])
def exit():
    response = counter_api.decrement("visitors")
    return f'{response.get("visitors")}', 201


if __name__ == "__main__":
    counter_api.API_PORT = sys.argv[1]
    counter_api.API_IP = sys.argv[2]
    app.run(host="0.0.0.0", port=5001)

import requests

API_IP = None
API_PORT = None


def increment(topic) -> dict:
    response = requests.post(f"http://{API_IP}:{API_PORT}/increment/{topic}")
    return response.json()


def decrement(topic) -> dict:
    response = requests.post(f"http://{API_IP}:{API_PORT}/decrement/{topic}")
    return response.json()


def get(topic) -> dict:
    response = requests.get(f"http://{API_IP}:{API_PORT}/{topic}")
    return response.json()

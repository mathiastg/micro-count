## Tasks

- [ ] remove SVG service, its not needed. Cached by browser, I THINK, otherwise test it a bit more. 
- [ ] implement better gle-icon AND RENAME THE DAMN THING
- [ ] implement menu from slack conversation.
- [ ] make the menu scaleable to screensize/dimensions etc.
- [ ] implement a overlay where the menu appears.
- [ ] implement a listener for TouchMove events that evaluates a circle
- [ ] make the overlay appear when the circle is asserted. Maybe change to a
      double circle within the same TouchStarted/Down/ event
- [ ] make the overlay appear at the center of the drawn circle
- [ ] make the menu be dynamic to number of menu entries


to fix menu

- [ ] --item-count is set manually in the html. this should probably be more dynamic.
- [ ] same with --item-total - it might be fine like this, but it can probably be improved with scss
- [ ] Generally clean up CSS so we are actually able to read the thing

chop it into pieces so we can decide more things on input, like: rotation left or right (+-360 links__item --angle var), circle symbols or not, background/foreground color according to theme, etc. (edited) 

next steps,
make it appear on on a layer above
make it appear when we draw a circle
make it appear at the center of the drawn circle
make it disappear when clicking outside the circle


react icon from ssr-client that works just fine: 
```
import { React } from "../../deps.ts";

const iconStyle: React.CSSProperties = {
  width: "100%",
  height: "100%",
  lineHeight: "0",
};

interface IconDef {
  namespace: string;
  name: string;
}

const Icon: React.FunctionComponent<IconDef> = (
  { namespace, name },
) => (
  <img
    style={iconStyle}
    src={`/public/assets/icons/${namespace}/${name}.svg`}
  >
  </img>
);
export default Icon;
```

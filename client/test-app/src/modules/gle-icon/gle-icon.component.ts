import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';

@Component({
  selector: 'gle-icon',
  templateUrl: './gle-icon.component.html',
  styleUrls: ['./gle-icon.component.scss'],
})
/**
 * namespace refers to a subfolder under /assets/icons
 * Name refers to the filename af a file (without file extension)
 *
 * undefined namespace will assume the file is placed at /assets/icons
 * use fill css property to change color of svg
 *
 */
export class GleIconComponent implements OnInit,OnChanges {
  @Input() name: string;
  @Input() namespace?: string = undefined;

  public path:string
  constructor() {}
  ngOnInit(): void {
    this.path=`/assets/icons/${this.namespace}/${this.name}.svg`
  }
  ngOnChanges(changes: SimpleChanges): void {
      this.path=`/assets/icons/${this.namespace}/${this.name}.svg`
  }
}

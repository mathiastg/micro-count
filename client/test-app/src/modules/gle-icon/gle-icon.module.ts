import { APP_INITIALIZER, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GleIconComponent } from './gle-icon.component';
import { HttpClientModule } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';

@NgModule({
  declarations: [GleIconComponent],
  imports: [CommonModule, HttpClientModule],
  exports: [GleIconComponent],
})
export class GleIconModule {}

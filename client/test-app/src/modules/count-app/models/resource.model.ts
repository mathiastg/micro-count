export interface ResourceRecord {
  resource: string;
  count: number;
}

export function isResourceRecord(obj: any): obj is ResourceRecord {
  return (
    !!obj &&
    obj !== undefined &&
    obj?.resource !== undefined &&
    obj?.count !== undefined
  );
}

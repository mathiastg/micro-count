import { APP_INITIALIZER, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountAppComponent } from './components/count-app.component';
import {
  ApplicationLoader,
  ApplicationLoaderModule,
} from '@gleerup/application-loader';

@NgModule({
  declarations: [CountAppComponent],
  imports: [CommonModule, ApplicationLoaderModule],
  exports: [CountAppComponent],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: (loader: ApplicationLoader) => () => {
        loader.registerApp({
          path: 'count',
          iconName: { name: 'layout-2', namespace: 'layouts' },
          component: CountAppComponent,
          title: 'CountApp',
          priority: 1,
        });
      },
      deps: [ApplicationLoader],
      multi: true,
    },
  ],
})
export class CountAppModule {}

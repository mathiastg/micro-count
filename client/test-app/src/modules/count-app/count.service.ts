import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, filter, map, Observable, of, tap } from 'rxjs';
import { isResourceRecord, ResourceRecord } from './models/resource.model';

@Injectable({
  providedIn: 'root',
})
export class CountService {
  variable:number = 15;
  holder:Observable<ResourceRecord | undefined>=of(undefined)
  private subject: BehaviorSubject<ResourceRecord | undefined> =
    new BehaviorSubject<ResourceRecord | undefined>(undefined);
  constructor(private restApi: HttpClient) {}

  public increment(resource: string): Observable<ResourceRecord> {
    return this.restApi
      .post(`/count-service/increment/${resource}`,undefined)
      .pipe(
        filter((body:any):body is ResourceRecord => isResourceRecord(body)),
        tap((record:ResourceRecord) => this.subject.next(record))
      );
  }

  public get record$(): Observable<ResourceRecord | undefined> {
    return this.subject.asObservable();
  }
  public set record$(thing:Observable<ResourceRecord | undefined>) {
    this.holder=thing
  }
}

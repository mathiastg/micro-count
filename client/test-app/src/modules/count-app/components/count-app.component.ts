import { Component, OnInit } from '@angular/core';
import { filter, Observable } from 'rxjs';
import { CountService } from '../count.service';
import { isResourceRecord, ResourceRecord } from '../models/resource.model';

@Component({
  selector: 'app-main',
  templateUrl: './count-app.component.html',
  styleUrls: ['./count-app.component.scss'],
})
export class CountAppComponent implements OnInit {
  constructor(private countService: CountService) {}

  ngOnInit(): void {}

  public increment() {
    this.countService.increment('newresource').subscribe();
  }
  public latestResource(): Observable<ResourceRecord> {
    return this.countService.record$.pipe(filter(isResourceRecord));
  }
}

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { autoMock, JasmineMock } from 'src/tools/mock';
import { CountService } from '../count.service';
import { of } from 'rxjs';

import { CountAppComponent } from './count-app.component';

class Thing {
  nv:number
  her:string
}

describe('MainComponent', () => {
  let component: CountAppComponent;
  let fixture: ComponentFixture<CountAppComponent>;
  let mockCountService: JasmineMock<CountService>

  beforeEach(() => {
    mockCountService=autoMock(CountService)
    mockCountService.record$=of({resource:"hey",count:1})

    TestBed.configureTestingModule({
      imports:[HttpClientTestingModule],
      declarations: [CountAppComponent],
    });
    fixture = TestBed.createComponent(CountAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', (done) => {
    mockCountService.record$.subscribe((value)=>{
      expect(value?.count).toBe(1)
      done()
    })
  });
});

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-no-open-app',
  templateUrl: './no-open-app.component.html',
  styleUrls: ['./no-open-app.component.scss']
})
export class NoOpenAppComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

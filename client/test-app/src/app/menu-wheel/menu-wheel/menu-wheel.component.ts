import { Component, OnInit } from '@angular/core';

export interface MenuEntry {
  icon:{name:string,namespace:string}
  text:string
  href:string
}

@Component({
  selector: 'app-menu-wheel',
  templateUrl: './menu-wheel.component.html',
  styleUrls: ['./menu-wheel.component.scss']
})
export class MenuWheelComponent implements OnInit {

  constructor() { }
  public thingArr=[
    {
    icon:{name:"expand-down",namespace:"navigation"},
    text:"thing-down",
    href:"#"
  },
  {
    icon:{name:"expand-alt",namespace:"navigation"},
    text:"thing-alt",
    href:"#"
  },
  {
    icon:{name:"expand-up",namespace:"navigation"},
    text:"thing-up",
    href:"#"
  },
  {
    icon:{name:"collapse-alt",namespace:"navigation"},
    text:"collapse-alt",
    href:"#"
  },
  {
    icon:{name:"collapse-up",namespace:"navigation"},
    text:"collapse-up",
    href:"#"
  }
]

  ngOnInit(): void {
  }

}

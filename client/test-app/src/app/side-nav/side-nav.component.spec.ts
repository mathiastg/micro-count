import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppEntry, ApplicationLoader } from '@gleerup/application-loader';
import { Observable, of } from 'rxjs';
import { autoMock, JasmineMock } from 'src/tools/mock';

import { SideNavComponent } from './side-nav.component';


class mockAppLoader extends ApplicationLoader{
  registerApp(app: AppEntry): void {
    throw new Error('Method not implemented.');
  }
  getApps(): Observable<AppEntry[]> {
    throw new Error('Method not implemented.');
  }
  constructor(){
    super()
  }
}

describe('SideNavComponent', () => {
  let component: SideNavComponent;
  let fixture: ComponentFixture<SideNavComponent>;
  let mockApplicationLoader: JasmineMock<ApplicationLoader>;

  beforeEach(() => {
    mockApplicationLoader=autoMock(mockAppLoader)
    mockApplicationLoader.getApps.and.returnValue(of())
    TestBed.configureTestingModule({
      declarations: [ SideNavComponent ],
      schemas:[CUSTOM_ELEMENTS_SCHEMA],
      providers:[{provide:ApplicationLoader,useValue:mockApplicationLoader}]
    })
    .compileComponents();
    fixture = TestBed.createComponent(SideNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

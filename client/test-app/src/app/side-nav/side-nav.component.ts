import { Component, OnInit } from '@angular/core';
import { map, Observable } from 'rxjs';
import { AppEntry, ApplicationLoader } from '@gleerup/application-loader';

export interface SideNavEntry {
  iconName: string;
  title: string;
}

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss'],
})
export class SideNavComponent implements OnInit {
  constructor(private readonly loader: ApplicationLoader) {}
  public navi="expand-right"

  ngOnInit(): void {}

  public appEntries(): Observable<AppEntry[]> {
    return this.loader.getApps().pipe(
      map((apps) =>
        apps.sort((a, b) => {
          if (a.priority === undefined && b.priority !== undefined) {
            return 1;
          }
          if (b.priority === undefined && a.priority !== undefined) {
            return -1;
          }
          return (
            (!!a.priority ? a.priority : -100) -
            (!!b.priority ? b.priority : 100)
          );
        })
      )
    );
  }
  public switchNavi(){
    if (this.navi==="expand-left"){
      this.navi="expand-right"
    }else{
      this.navi="expand-left"
    }
  }
}

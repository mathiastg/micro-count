import { ApplicationModule, APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import { NoOpenAppComponent } from './no-open-app/no-open-app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GleIconModule } from 'src/modules/gle-icon/gle-icon.module';
import { CountAppModule } from 'src/modules/count-app/count-app.module';
import {
  ApplicationLoader,
  ApplicationLoaderModule,
} from '@gleerup/application-loader';
import { MenuWheelComponent } from './menu-wheel/menu-wheel/menu-wheel.component';

@NgModule({
  declarations: [
    AppComponent,
    TopBarComponent,
    SideNavComponent,
    NoOpenAppComponent,
    MenuWheelComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    GleIconModule,
    CountAppModule,
    ApplicationModule,
    ApplicationLoaderModule,
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: (loader: ApplicationLoader) => () => {
        loader.registerApp({
          path: '',
          iconName: { name: 'layout-1', namespace: 'layouts' },
          component: NoOpenAppComponent,
          title: 'NoApp',
          priority: 0,
        });
      },
      deps:[ApplicationLoader],
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

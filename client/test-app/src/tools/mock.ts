type PropertyDescriptorCollection = {
  [x: string]: PropertyDescriptor;
};

function isFunction(props: PropertyDescriptorCollection, field: string) {
  return (
    props[field]?.get === undefined &&
    props[field]?.set === undefined &&
    props[field]?.value !== undefined
  );
}

export type JasmineMock<T> = T & {
  [k in keyof T]: T[k] extends Function ? jasmine.Spy & T[k] : T[k];
};

const CONSTRUCTOR = 'constructor';

type Constructor<T> = new (...args: any[]) => T;
type AbstractConstructor<T> = abstract new (...args: any[]) => T;

export function autoMock<Type>(obj: Constructor<Type>): JasmineMock<Type> {
  const res: JasmineMock<Type> = {} as any;

  const functionKeys = Object.getOwnPropertyNames(obj.prototype);

  const propertyDescriptor = Object.getOwnPropertyDescriptors(obj.prototype);

  functionKeys.forEach((key) => {
    if (key === CONSTRUCTOR) {
      return;
    }
    if (isFunction(propertyDescriptor, key)) {
      //determine whether this is a function or a get/set property
      (res as any)[key] = jasmine.createSpy(key);
    } else {
      Object.defineProperty(res, key, {
        configurable: propertyDescriptor[key].configurable,
        enumerable: propertyDescriptor[key].enumerable,
        get: () => (res as any)[`_mock_${key}`],
        set: (value) => ((res as any)[`_mock_${key}`] = value),
      });
    }
  });

  return res as JasmineMock<Type>;
}

// export function mockAbstract<Type>(obj: AbstractConstructor<Type>) {

//   const res: JasmineMock<Type> = {} as any;

//   const functionKeys = Object.getOwnPropertyNames(obj);

//   const propertyDescriptor = Object.getOwnPropertyDescriptors(obj);

//   functionKeys.forEach((key) => {
//     if (key === CONSTRUCTOR) {
//       return;
//     }
//     if (isFunction(propertyDescriptor, key)) {
//       //determine whether this is a function or a get/set property
//       (res as any)[key] = jasmine.createSpy(key);
//     } else {
//       Object.defineProperty(res, key, {
//         configurable: propertyDescriptor[key].configurable,
//         enumerable: propertyDescriptor[key].enumerable,
//         get: () => (res as any)[`_mock_${key}`],
//         set: (value) => ((res as any)[`_mock_${key}`] = value),
//       });
//     }
//   });
//   console.log(res)

//   return res as JasmineMock<Type>;
// }

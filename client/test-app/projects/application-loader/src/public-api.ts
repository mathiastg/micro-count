/*
 * Public API Surface of application-loader
 */

export {
  ApplicationLoader,
  AppEntry,
} from './lib/public/application-loader.interface';
export { ApplicationLoaderModule } from './lib/application-loader.module';

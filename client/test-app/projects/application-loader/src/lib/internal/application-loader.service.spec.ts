import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ApplicationLoaderService } from './application-loader.service';

describe('ApplicationLoaderService', () => {
  let service: ApplicationLoaderService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[RouterTestingModule]
    });
    service = TestBed.inject(ApplicationLoaderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

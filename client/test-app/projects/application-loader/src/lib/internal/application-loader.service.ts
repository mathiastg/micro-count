import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import {
  AppEntry,
  ApplicationLoader,
} from '../public/application-loader.interface';

@Injectable({
  providedIn: 'root',
})
export class ApplicationLoaderService extends ApplicationLoader {
  private apps: BehaviorSubject<AppEntry[]> = new BehaviorSubject<AppEntry[]>(
    []
  );
  constructor(private readonly router: Router) {
    super();
  }
  registerApp(app: AppEntry): void {
    this.router.config.push({ path: app.path, component: app.component });
    this.apps.next([...this.apps.value, app]);
  }
  getApps(): Observable<AppEntry[]> {
    return this.apps.asObservable();
  }
}

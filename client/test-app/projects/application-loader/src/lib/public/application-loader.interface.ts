import { Type } from '@angular/core';
import { Observable } from 'rxjs';

export abstract class ApplicationLoader {
  abstract registerApp(app: AppEntry): void;
  abstract getApps(): Observable<AppEntry[]>;
}

export interface AppEntry {
  iconName: { name: string; namespace?: string };
  title: string;
  path: string;
  component: Type<any>;
  priority?: number;
}

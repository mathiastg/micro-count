import { NgModule, Type } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApplicationLoader } from './public/application-loader.interface';
import { ApplicationLoaderService } from './internal/application-loader.service';
import { Router } from '@angular/router';

@NgModule({
  declarations: [],
  imports: [CommonModule],
  exports: [],
  providers: [
    {
      provide: ApplicationLoader,
      useFactory: (router: Router) => new ApplicationLoaderService(router),
      deps: [Router],
    },
  ],
})
export class ApplicationLoaderModule {}

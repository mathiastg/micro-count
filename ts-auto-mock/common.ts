type PropertyDescriptorCollection = {
  [x: string]: PropertyDescriptor;
};

export function isFunction(props: PropertyDescriptorCollection, field: string) {
  return (
    props[field]?.get === undefined &&
    props[field]?.set === undefined &&
    props[field]?.value !== undefined
  );
}
export const CONSTRUCTOR = "constructor";

// deno-lint-ignore-file ban-types no-explicit-any
import { sinon } from "./deps.ts";
import { CONSTRUCTOR, isFunction } from "./common.ts";
export type SinonMock<T> =
  & T
  & {
    [k in keyof T]: T[k] extends Function ? sinon.SinonSpy & T[k] : T[k];
  };

export function autoMock<Type>(
  obj: new (...args: any[]) => Type,
): SinonMock<Type> {
  const res: SinonMock<Type> = {} as any;

  const functionKeys = Object.getOwnPropertyNames(obj.prototype);

  const propertyDescriptors = Object.getOwnPropertyDescriptors(obj.prototype);

  functionKeys.forEach((key) => {
    if (key === CONSTRUCTOR) {
      return;
    }
    if (isFunction(propertyDescriptors, key)) { //determine whether this is a function or a get/set property
      (res as any)[key] = sinon.spy(key);
    } else { //if its a get set property we define it to refer to a different thing
      Object.defineProperty(res, key, {
        configurable: propertyDescriptors[key].configurable,
        enumerable: propertyDescriptors[key].enumerable,
        get: () => (res as any)[`_mock_${key}`],
        set: (value) => ((res as any)[`_mock_${key}`] = value),
      });
    }
  });

  return res as SinonMock<Type>;
}
